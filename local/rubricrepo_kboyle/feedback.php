<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the feedback form
 *
 * @package     local
 * @subpackage  rubricrepo_kboyle
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 * Example:
 * require_once('../../config.php');
 * global $CFG, $PAGE;
 *
 * $PAGE->set_context(context_system::instance());
 * $PAGE->set_pagelayout('standard');
 * $PAGE->set_title('Form name');
 * $PAGE->set_heading('Form name');
 * $PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_kboyle/view.php');
 * echo $OUTPUT->header();
**/

global $CFG, $PAGE, $DB;
require_once('../../config.php');

require_login();
require_capability('local/rubricrepo_kboyle:add', context_system::instance());
require_once($CFG->dirroot.'/local/rubricrepo_kboyle/feedback_form.php');

// This block handles the page generation and set up.
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_rubricrepo_kboyle'));
$PAGE->set_heading(get_string('pluginname', 'local_rubricrepo_kboyle'));
$PAGE->set_url($CFG->wwwroot.'/local/rubricrepo_kboyle/feedback.php');
$feedback_form = new create_feedback_instance($CFG->wwwroot.'/local/rubricrepo_kboyle/feedback.php?formid='.$_GET['formid']);

/* This block handles the logic for how to handle the form.
 * 1. If the form is cancelled, re-direct back to this page.
 * 2. If the form is submitted and the information is valid, then process the form.
 * 3. If there is no form information, then display the newly created form.
**/

$table1 = 'comments';

if ($feedback_form->is_cancelled()) {
	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/feedback.php?formid='.$_GET['formid']);
} elseif ($_SERVER['REQUEST_METHOD'] == 'POST' && $data =$feedback_form->get_data()) { //Double guard to ensure that the requesting method is POST and that form data is valid.
	//$check = $data->test1;
	//$check2 = $data->test2;
	//echo 'Form has data<br>';
	$var = $data->letssee;
	/*--------------------------------------------------------------------------- */
                        /*----------------------------Testing updating assignment grade -------------i*/

                        $grade_input = $feedback_form->get_data();
                        $grade_value = $grade_input->scale;
                        $forum_discussion = $DB->get_record('forum_discussions',array('firstpost'=>$_GET['id']));
                        $grade = new stdClass();
                        $grade->userid = 3;
                        $grade->timecreated = time();
                        $grade->timemodified = $grade->timecreated;
                        $grade->grader = $USER->id;
                        $grade->assignment = 4; //Need to grab assignment selection from user
                        $grade->grade = $grade_input->scale;
                        //$grade->grade = 85;  //Need to grab assignment grade from user
                        $checkIfGraded = $DB->get_record('assign_grades',array('userid'=>$grade->userid, 'assignment'=>$grade->assignment));


                        if($checkIfGraded){
                                $grade->id=$checkIfGraded->id;
                                $result = $DB->update_record('assign_grades',$grade);

                                } else {
                                        $result = $DB->insert_record('assign_grades', $grade);
                                }


                                $courseid = $forum_discussion->course;
                                $itemid = $DB->get_record('grade_items',array('iteminstance'=>$grade->assignment, 'courseid'=>$courseid));
                                $student = $DB->get_record('grade_grades',array('userid'=>$grade->userid, 'itemid'=>$itemid->id));

                                // Assigning grades at this point, need t be able to grab user input information.
                                $student->rawgrade = $grade->grade;
                                $student->finalgrade = $grade->grade;

                                $DB->update_record('grade_grades',$student);


	redirect($CFG->wwwroot.'/local/rubricrepo_kboyle/email.php'.$var);
	$grp_count = 0;
	$chkboxgrps = 'grp'.$grp_count;
	while (!empty($_POST[$chkboxgrps])) {
		//echo 'Checkboxes selected<br>';
		$checkbox = $_POST[$chkboxgrps];
		//echo $selected.'<br>';
		foreach($checkbox as $c) {
			//echo $c;
			if ($c != 0) {
				$result = $DB->get_record($table1, array('id'=>$c));
				echo $result->comment_text.'<br>';
			}
		}
		$grp_count++;
		$chkboxgrps = 'grp'.$grp_count;
	}
} else {
	//echo 'formid is'.$_GET['formid'].'<br>';
	//echo 'id is'.$_GET['id'].'<br>';
	//echo 'id as POST is'.$_POST['id'].'<br>';
	//echo 'courseid is'.$_GET['courseid'].'<br>';
	echo $OUTPUT->header();
	$feedback_form->display();
	echo $OUTPUT->footer();
}

?>