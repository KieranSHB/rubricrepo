<?php
require_once '../../config.php';
require_once $CFG->dirroot.'/lib/formslib.php';
require_once $CFG->dirroot.'/lib/datalib.php';

class js_form extends moodleform {

	function definition() {
		global $CFG, $DB; // These are required to be declared inside the constructor.
		$mform = $this->_form;

		$arrgroup = array('1'=>'Fruits', '2'=>'Names');

		$mform->addElement('select', 'parent', get_string('selectform', 'local_feedback_ec10'), $arrgroup);
		$mform->addElement('static', 'child', null, 'text');
	}

}
?>